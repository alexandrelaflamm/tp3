import sys
import argparse
import socket
import struct
import socketUtils
from datetime import datetime
from cryptoModule import entierAleatoire, trouverNombrePremier, exponentiationModulaire

try:

    file = open("Error.log", "w", encoding="utf8")

    # argparse pour l'option p
    parser = argparse.ArgumentParser(description="1er serveur en mode écoute.")
    parser.add_argument("-p", "--port", dest="port", type=int, action="store", required=True,
                        help="Choisir un port")
    groupe = parser.add_mutually_exclusive_group(required=True)
    groupe.add_argument("-e", "--ecoute", dest="listen", action="store_const", const="-e", default="",
                        help="Indiquer l’adresse de l’hôte")
    groupe.add_argument("-a", "--addresse", dest="address", action="store", default="",
                        help="Indiquer l’adresse de l’hôte. Ne peut être utiliser si -e est activé (en mode listen)")
    parser.add_argument("-6", dest="addresseType", action="store_const", const=socket.AF_INET6, default=socket.AF_INET,
                        help="Type de protocole IP (IPv4 par défaut; -6 donne IPv6)")


    port = parser.parse_args().port
    addresse = parser.parse_args().address
    addresseType = parser.parse_args().addresseType
    mode = parser.parse_args().listen

    if ((addresse != "::1" and addresseType == socket.AF_INET6 and not mode) or (addresse != "::1" and addresseType == socket.AF_INET6 and not mode)):
        parser.error("Erreur : mauvaise adresse utilisée")

    msgSize = 128
    numConnexion = 0
    line = ["~" for _ in range(79)]

    # On affiche le nombre de connexions au serveur

    if (mode == "-e"):  # mode serveur

        modulo = trouverNombrePremier()  # A
        base = entierAleatoire(modulo)  # B

        serverSocket = socket.socket(addresseType, socket.SOCK_STREAM) 

        # On passe l’option SO_REUSEADDR qui permet de réutiliser un socket #
        # (en particulier l’adresse) sans attendre qu’il expire
        serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # On indique au socket d’écouter sur l’adresse et le port selectionné
        serverSocket.bind((addresse, port))

        # On démarre le socket, indiquant qu’il peut mettre en attente
        # d’acceptation 5 connexions avant d’en rejeter
        serverSocket.listen(5)

        print(''.join(line))
        print("Démarrage du serveur...")
        print("Écoute sur le port              : " + str(port))

        while True: 
            # clientSocket est un nouveau socket pour intéragir avec le client
            # addr est l’adresse IP et le port du client, si on ne compte pas
            # s’en servir on peut remplacer la variable par _ pour l’ignorer

            (clientSocket, addr) = serverSocket.accept()

            numConnexion += 1
            print("Connexion n°" + str(numConnexion))

            print(''.join(line))
            print("Envoi du modulo                  : " + str(modulo))


            clientSocket.send(str(modulo).encode("utf8"))

            print("Envoi de la base                 : " + str(base))
            print(''.join(line))

            clientSocket.send(str(base).encode("utf8"))

            serverPrivKey = entierAleatoire(modulo) 
            serverPubKey = exponentiationModulaire(base, serverPrivKey, modulo)
            print("Clé privée                       : " + str(serverPrivKey))
            print("Clé publique à envoyer           : " + str(serverPubKey))
            clientSocket.send(str(serverPubKey).encode("utf8"))

            clientPubKey = int(clientSocket.recv(msgSize).decode("utf8"))
            print("Clé publique reçue               : " + str(clientPubKey))
            serverShareKey = exponentiationModulaire(
                clientPubKey, serverPrivKey, modulo)

            print("Clé partagée                     : " + str(serverShareKey))
            print(''.join(line))

        clientSocket.close()

    elif (not mode):  # mode client
        # Le socke aura besoin d’un tuple contenant l’adresse et le port
        
        destination = (addresse, port)

        # Création du socket et connexion au serveur
        soc = socket.socket(addresseType, socket.SOCK_STREAM)
        soc.connect(destination)  

        print(''.join(line))
        modulo = int(soc.recv(msgSize).decode(encoding="utf8"))
        print("Réception du modulo              : " + str(modulo))


        base = int(soc.recv(msgSize).decode(encoding="utf8"))
        print("Réception de la base             : " + str(base))
        print(''.join(line))

        clientPrivKey = entierAleatoire(modulo)
        clientPubKey = exponentiationModulaire(base, clientPrivKey, modulo)

        print("Clé privée                       : " + str(clientPrivKey))
        print("Clé publique à envoyer           : " +
              str(clientPubKey))

        serverPubKey = int(soc.recv(msgSize).decode("utf8"))
        print("Clé publique reçue               : " +
              str(serverPubKey))

        soc.send(str(clientPubKey).encode(encoding="utf8"))

        clientShareKey = exponentiationModulaire(
            serverPubKey, clientPrivKey, modulo)

        print("Clé partagée                     : " + str(clientShareKey))
        print(''.join(line))


        soc.close()


except ConnectionRefusedError as errConnectionRefused:
    message = (datetime.now().__str__() + " Erreur : La connection a été refusée \n" + errConnectionRefused.__str__())
    print(message)
    file.write(message)  
    file.close()

except ConnectionAbortedError as errConnectionAborted:
    message = (datetime.now().__str__() + " Erreur : La connexion avec le serveur a été interrompue \n " + errConnectionAborted.__str__())
    print(message)
    file.write(message)  
    file.close()

except ConnectionError as errConnection:
    message = (datetime.now().__str__() + " Erreur : Problème de connexion avec le serveur \n " + errConnection.__str__())
    print(message)
    file.write(message)  
    file.close()

except OSError as errOS:
    message = (datetime.now().__str__() + " : " + errOS.__str__())
    print(message)
    file.write(message)  
    file.close()

except BaseException as errBase:
    message = datetime.now().__str__() + " : " + errBase.__str__()
    file.write(message)
    file.close()


